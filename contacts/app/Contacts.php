<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $fillable = [
    	'avatar',
        'first_name', 
        'last_name',
        'address', 
        'city',
        'zip', 
        'country',
        'email',
        'phone',
        'note',
        'group'
    ];
}
