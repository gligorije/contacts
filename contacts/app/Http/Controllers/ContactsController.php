<?php

namespace App\Http\Controllers;

use App\Contacts;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contacts::latest()->paginate(5);  
        return view('contacts.index',compact('contacts'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

   /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' =>  ['required', 'digits:10'],
            'group' => 'required',
        ]);
  
        Contacts::create($request->all());
   
        return redirect()->route('contacts.index')
                        ->with('success','Contact created successfully.');
    }
   
    /**
     * Display the specified resource.
     *
     * @param  \App\Contacts  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contacts $contact)
    {
        return view('contacts.show',compact('contact'));
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param   \App\Contacts  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contacts $contact)
    {
        return view('contacts.edit',compact('contact'));
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   \App\Contacts  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contacts $contact)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => ['required', 'digits:10'],
            'group' => 'required',
        ]);
  
        $contact->update($request->all());
  
        return redirect()->route('contacts.index')
                        ->with('success','Contact updated successfully');
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param   \App\Contacts  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contacts $contact)
    {
        $contact->delete();
  
        return redirect()->route('contacts.index')
                        ->with('success','Contact deleted successfully');
    }
}
